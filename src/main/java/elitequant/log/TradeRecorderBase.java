package elitequant.log;

import elitequant.order.FillEvent;

/**
 * transaction recorder
 * 
 */
public abstract class TradeRecorderBase {
	
	/**
	 * logs fill event
	 * 
	 * @param fill
	 */
	public abstract void recordTrade(FillEvent fill);

}
