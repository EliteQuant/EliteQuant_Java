package elitequant;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.yaml.snakeyaml.Yaml;

import elitequant.brokerage.BacktestBrokerage;
import elitequant.data.BacktestDataFeedLocal;
import elitequant.data.BacktestDataFeedQuandl;
import elitequant.data.BacktestDataFeedTushare;
import elitequant.data.BarEvent;
import elitequant.data.DataBoard;
import elitequant.data.DataFeedBase;
import elitequant.data.TickEvent;
import elitequant.event.BacktestEventEngine;
import elitequant.event.Event;
import elitequant.event.EventHandler;
import elitequant.event.EventType;
import elitequant.event.EventsEngine;
import elitequant.order.FillEvent;
import elitequant.order.OrderEvent;
import elitequant.performance.PerformanceManager;
import elitequant.position.PortfolioManager;
import elitequant.risk.PassThroughRiskManager;
import elitequant.risk.RiskManagerBase;
import elitequant.strategy.StrategyBase;

/**
 * Event driven backtest engine
 * 
 */
public class BacktestEngine {

	private BigDecimal initialCash;

	private String[] symbols;

	private String benchmark;

	private String startDate;

	private String endDate;

	private String strategyName;

	private String datasource;

	private String histDir;

	private String outputDir;

	private DataFeedBase dataFeed;

	private BacktestEventEngine eventsEngine;

	private DataBoard dataBoard;

	private BacktestBrokerage backtestBrokerage;

	private PortfolioManager portfolioManager;

	private PerformanceManager performanceManager;

	private RiskManagerBase riskManager;

	private StrategyBase strategy;

	private Date currentTime;

	/**
	 * read in configs Set up backtest event engine
	 * 
	 * @param config
	 */
	public BacktestEngine(Config config) {

		// read in configs
		initialCash = config.getCash();
		symbols = config.getTickers();
		benchmark = config.getBenchmark();
		startDate = config.getStart_date();
		endDate = config.getEnd_date();
		strategyName = config.getStrategy();
		datasource = config.getDatasource();
		histDir = config.getHist_dir();
		outputDir = config.getOutput_dir();

		// data_feed
		List<String> symbols_all = Arrays.asList(symbols);
		if (benchmark != null && benchmark.trim().length() > 0) {
			symbols_all.add(benchmark);
		}

		if ("LOCAL".equals(datasource.toUpperCase())) {
			dataFeed = new BacktestDataFeedLocal(histDir, startDate, endDate);
		} else if ("TUSHARE".equals(datasource.toUpperCase())) {
			dataFeed = new BacktestDataFeedTushare(startDate, endDate);
		} else {
			dataFeed = new BacktestDataFeedQuandl(startDate, endDate);
		}

		dataFeed.subscribeMarketData(symbols);

		// event engine
		eventsEngine = new BacktestEventEngine(dataFeed);

		// brokerage
		dataBoard = new DataBoard();
		backtestBrokerage = new BacktestBrokerage(eventsEngine, dataBoard);

		// portfolio_manager
		portfolioManager = new PortfolioManager(initialCash);

		// performance_manager
		performanceManager = new PerformanceManager(symbols_all.toArray(new String[symbols_all.size()]));

		// risk_manager
		riskManager = new PassThroughRiskManager();

		// load all strategies
		try {
			Class strategyClass = Class.forName(String.format("elitequant.strategy.mystrategy.%s", strategyName));
			strategy = (StrategyBase) strategyClass.getConstructor(String[].class, EventsEngine.class)
					.newInstance(symbols, eventsEngine);
		} catch (Exception e) {
			System.out.println(String.format("can not find strategy：%s", strategyName));
			System.exit(0);
		}

		eventsEngine.registerHandler(EventType.TICK, new TickEventHandler());
		eventsEngine.registerHandler(EventType.BAR, new BarEventHandler());
		eventsEngine.registerHandler(EventType.ORDER, new OrderEventHandler());
		eventsEngine.registerHandler(EventType.FILL, new FillEventHandler());

	}

	private class TickEventHandler implements EventHandler {

		@Override
		public void handle(Event event) {
			// if not TickEvent, ignore
			if (!TickEvent.class.isInstance(event)) {
				return;
			}

			TickEvent tickEvent = (TickEvent) event;
			currentTime = tickEvent.timestamp;

			// performance update goes before position updates because it
			// updates previous day
			performanceManager.updatePerformance(currentTime, portfolioManager, dataBoard);
			portfolioManager.markToMarket(currentTime, tickEvent.fullSymbol, tickEvent.price);
			dataBoard.onTick(tickEvent);
			strategy.onTick(tickEvent);

		}
	}

	private class BarEventHandler implements EventHandler {

		@Override
		public void handle(Event event) {
			// if not BarEvent, ignore
			if (!BarEvent.class.isInstance(event)) {
				return;
			}

			BarEvent barEvent = (BarEvent) event;
			currentTime = barEvent.barEndTime();

			// performance update goes before position updates because it
			// updates previous day
			performanceManager.updatePerformance(currentTime, portfolioManager, dataBoard);
			portfolioManager.markToMarket(currentTime, barEvent.fullSymbol, barEvent.adjClosePrice);
			dataBoard.onBar(barEvent);
			strategy.onBar(barEvent);

		}
	}

	private class OrderEventHandler implements EventHandler {

		@Override
		public void handle(Event event) {

			// if not OrderEvent, ignore
			if (!OrderEvent.class.isInstance(event)) {
				return;
			}

			OrderEvent orderEvent = (OrderEvent) event;
			backtestBrokerage.placeOrder(orderEvent);

		}
	}

	private class FillEventHandler implements EventHandler {

		@Override
		public void handle(Event event) {

			// if not FillEvent, ignore
			if (!FillEvent.class.isInstance(event)) {
				return;
			}

			FillEvent fillEvent = (FillEvent) event;

			portfolioManager.onFill(fillEvent);
			performanceManager.onFill(fillEvent);

		}
	}

	/**
	 * Run backtest
	 */
	public void run() {
		eventsEngine.run();
		performanceManager.updateFinalPerformance(currentTime, portfolioManager, dataBoard);
		performanceManager.saveResults(outputDir);
		performanceManager.createTearsheet();

	}

	public static class Config {

		private String strategy;

		private BigDecimal cash;

		private String[] tickers;

		private String benchmark;

		private String start_date;

		private String end_date;

		private String datasource;

		private String hist_dir;

		private String output_dir;

		public String getStrategy() {
			return strategy;
		}

		public void setStrategy(String strategy) {
			this.strategy = strategy;
		}

		public BigDecimal getCash() {
			return cash;
		}

		public void setCash(BigDecimal cash) {
			this.cash = cash;
		}

		public String[] getTickers() {
			return tickers;
		}

		public void setTickers(String[] tickers) {
			this.tickers = tickers;
		}

		public String getBenchmark() {
			return benchmark;
		}

		public void setBenchmark(String benchmark) {
			this.benchmark = benchmark;
		}

		public String getStart_date() {
			return start_date;
		}

		public void setStart_date(String start_date) {
			this.start_date = start_date;
		}

		public String getEnd_date() {
			return end_date;
		}

		public void setEnd_date(String end_date) {
			this.end_date = end_date;
		}

		public String getDatasource() {
			return datasource;
		}

		public void setDatasource(String datasource) {
			this.datasource = datasource;
		}

		public String getHist_dir() {
			return hist_dir;
		}

		public void setHist_dir(String hist_dir) {
			this.hist_dir = hist_dir;
		}

		public String getOutput_dir() {
			return output_dir;
		}

		public void setOutput_dir(String output_dir) {
			this.output_dir = output_dir;
		}
	}

	public static void main(String[] args) {
	    
        // Set the swing for english
        Locale.setDefault(Locale.ENGLISH);
        
		Config config = null;
		Yaml yaml = new Yaml();
		try (FileInputStream yamlInput = new FileInputStream("config_backtest.yaml")) {
			config = yaml.loadAs(yamlInput, Config.class);
		} catch (IOException e) {
			System.out.println("config_backtest.yaml is missing");
		}

		BacktestEngine backtestEngine = new BacktestEngine(config);
		backtestEngine.run();

	}

}
