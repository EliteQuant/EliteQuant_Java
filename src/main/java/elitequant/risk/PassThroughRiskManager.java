package elitequant.risk;

import elitequant.order.OrderEvent;

public class PassThroughRiskManager extends RiskManagerBase {

	/**
	 * Pass through the order without constraints
	 */
	@Override
	public OrderEvent orderInCompliance(OrderEvent originalOrder) {
		return originalOrder;

	}

}
