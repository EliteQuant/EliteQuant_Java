package elitequant;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;

import org.yaml.snakeyaml.Yaml;

import elitequant.gui.MainWindow;

public class LiveEngine {
	
	private static Process serverProcess;
	
	static class StartServer{
		
		public static void main(String[] args){
			System.out.println("Running java server.");
			// TODO: 这里需要调用交易引擎(EliteQuant.pyd)
			/*
			 *  server = tradingengine_() 
			 *  server.run()
			 */
			
		}
	}
	
	static class StopServer extends Thread{

		@Override
		public void run() {
			serverProcess.destroy();
		}
		
	}
	
	public static class Config {

		private String broker;
		private String msgq;
		private String log_dir;
		private String data_dir;
		private String account;
		private String ibport;
		private String ctp_broker_id;
		private String ctp_user_id;
		private String ctp_password;
		private String ctp_md_address;
		private String ctp_td_address;
		private String[] tickers;
		
		public Config(){}

		public String getBroker() {
			return broker;
		}

		public void setBroker(String broker) {
			this.broker = broker;
		}

		public String getMsgq() {
			return msgq;
		}

		public void setMsgq(String msgq) {
			this.msgq = msgq;
		}

		public String getLog_dir() {
			return log_dir;
		}

		public void setLog_dir(String log_dir) {
			this.log_dir = log_dir;
		}

		public String getData_dir() {
			return data_dir;
		}

		public void setData_dir(String data_dir) {
			this.data_dir = data_dir;
		}

		public String getAccount() {
			return account;
		}

		public void setAccount(String account) {
			this.account = account;
		}

		public String getIbport() {
			return ibport;
		}

		public void setIbport(String ibport) {
			this.ibport = ibport;
		}

		public String getCtp_broker_id() {
			return ctp_broker_id;
		}

		public void setCtp_broker_id(String ctp_broker_id) {
			this.ctp_broker_id = ctp_broker_id;
		}

		public String getCtp_user_id() {
			return ctp_user_id;
		}

		public void setCtp_user_id(String ctp_user_id) {
			this.ctp_user_id = ctp_user_id;
		}

		public String getCtp_password() {
			return ctp_password;
		}

		public void setCtp_password(String ctp_password) {
			this.ctp_password = ctp_password;
		}

		public String getCtp_md_address() {
			return ctp_md_address;
		}

		public void setCtp_md_address(String ctp_md_address) {
			this.ctp_md_address = ctp_md_address;
		}

		public String getCtp_td_address() {
			return ctp_td_address;
		}

		public void setCtp_td_address(String ctp_td_address) {
			this.ctp_td_address = ctp_td_address;
		}

		public String[] getTickers() {
			return tickers;
		}

		public void setTickers(String[] tickers) {
			this.tickers = tickers;
		}

	}

	public static void main(String[] args) throws Exception {
	    
        // Set the swing for english
        Locale.setDefault(Locale.ENGLISH);
        
		ProcessBuilder pb = new ProcessBuilder("java", "elitequant.LiveEngine.StartServer");
		pb.redirectErrorStream(false);
		serverProcess = pb.start();
		Runtime.getRuntime().addShutdownHook(new StopServer());
		
		Config config = null;
		Yaml yaml = new Yaml();
		try (FileInputStream yamlInput = new FileInputStream("config.yaml")) {
			config = yaml.loadAs(yamlInput, Config.class);
		} catch (IOException e) {
			System.out.println("config.yaml is missing");
		}

		MainWindow mainWindow = new MainWindow(config);
		

	}

}
