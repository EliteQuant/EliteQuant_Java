package elitequant.brokerage;

import java.math.BigDecimal;

import elitequant.order.OrderEvent;

/**
 * Brokerage base class
 * 
 * @author btcaimail@163.com
 *
 */
public abstract class BrokerageBase {
	
	public void placeOrder(OrderEvent orderEvent) {
	    throw new Error("Implement this in your derived class");
	}
	
	public void cancelOrder(String orderId) {
        throw new Error("Implement this in your derived class");
	}
	
	public int nextOrderId() {
        throw new Error("Implement this in your derived class");
	}
	
	public BigDecimal calculateCommission(String fullSymbol, BigDecimal fillPrice, int fillSize) {
        throw new Error("Implement this in your derived class");
	}

}
