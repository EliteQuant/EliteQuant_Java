package elitequant.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.DateTickUnitType;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class TearsheetWindow extends JFrame {

	private static final long serialVersionUID = -2965765806864577384L;
	
	private List<Date> dates;
	private List<BigDecimal> moneys;

	public TearsheetWindow(List<Date> dates, List<BigDecimal> moneys) {
		super("Frame1");

		setLayout(new GridLayout(2, 1));
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        
		this.dates = dates;
		this.moneys = moneys;

		JFreeChart chart = ChartFactory.createTimeSeriesChart("Capital", null, "Capital",
		        createCapitalDataset(), true, true, false);

        chart.setBorderVisible(false);  
        chart.setBackgroundPaint(null);  
        chart.setBackgroundImageAlpha(0.0f);  

		XYPlot xyPlot = chart.getXYPlot();
		xyPlot.setForegroundAlpha(0.5f);
		xyPlot.setBackgroundAlpha(0.0f);  
		xyPlot.setOutlinePaint(null);  
		DateAxis domainAxis = (DateAxis)xyPlot.getDomainAxis();
		domainAxis.setTickUnit(new DateTickUnit(DateTickUnitType.YEAR, 1, new SimpleDateFormat("yyyy")));
		chart.getLegend().setVisible(false);

		// the second chart
        JFreeChart chart2 = ChartFactory.createTimeSeriesChart("Profit", null, "Profit",
                createProfitDataset(), true, true, false);

        chart2.setBorderVisible(false);  
        chart2.setBackgroundPaint(null);  
        chart2.setBackgroundImageAlpha(0.0f); 
        
        XYPlot xyPlot2 = chart2.getXYPlot();
        xyPlot2.setForegroundAlpha(0.5f);
        xyPlot2.setBackgroundAlpha(0.0f);  
        xyPlot2.setOutlinePaint(null);  
        DateAxis domainAxis2 = (DateAxis)xyPlot2.getDomainAxis();
        domainAxis2.setTickUnit(new DateTickUnit(DateTickUnitType.YEAR, 1, new SimpleDateFormat("yyyy")));
        chart2.getLegend().setVisible(false);
		
		ChartPanel chartPanel = new ChartPanel(chart);
        ChartPanel chartPanel2 = new ChartPanel(chart2);
		add(chartPanel);
        add(chartPanel2);
	}

    private TimeSeriesCollection createCapitalDataset() {
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        TimeSeries series = new TimeSeries("Capital");
        for(int i=0;i<dates.size();i++) {
            series.add(new Day(dates.get(i)), moneys.get(i));
        }

        dataset.addSeries(series);
        return dataset;
    }

    private TimeSeriesCollection createProfitDataset() {
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        TimeSeries series = new TimeSeries("Profit");
        
        List<BigDecimal> change = calChange();
        // 1 != 0 to ignore dates.get(0) 
        for(int i=1;i<dates.size();i++) {
            series.add(new Day(dates.get(i)), change.get(i-1));
        }

        dataset.addSeries(series);
        return dataset;
    }
	
	// calculate change
	private List<BigDecimal> calChange() {

		List<BigDecimal> change = new ArrayList<BigDecimal>();
		BigDecimal preMoney = moneys.get(0);
		for (int i = 1; i < moneys.size(); i++) {
			BigDecimal curMoney = moneys.get(i);
			// change percent
			change.add(curMoney.subtract(preMoney).divide(preMoney, 4, BigDecimal.ROUND_HALF_EVEN).multiply(new BigDecimal(100)));
			preMoney = curMoney;
		}
		
		return change;
	}

}
