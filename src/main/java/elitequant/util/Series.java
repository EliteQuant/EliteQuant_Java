package elitequant.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Series<K, V> extends HashMap<K, V> {

	private static final long serialVersionUID = -2877338110546720335L;
	
	private List<K> keyList;
	
	public Series(){
		super();
		keyList = new ArrayList<K>();
	}

	@Override
	public V put(K key, V value) {
		if(!containsKey(key)) {
			keyList.add(key);
		}
		return super.put(key, value);
	}

	public List<K> getKeyList() {
		return keyList;
	}

	public List<V> getValueList() {
		List<V> valueList = new ArrayList<V>();
		keyList.forEach(key ->{
			valueList.add(get(key));
		});
		return valueList;
	}
	
	
	
	
	

}
