package elitequant.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class UtilFunc {

    public static BigDecimal retrieveMultiplierFromFullSymbol(String fullSymbol) {
        return new BigDecimal(1.0);
    }
    
    public static Date getDate19700101() {
        Date d = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
            d = sdf.parse("1970-01-01 00:00:00");
        } catch (Exception e) {
            // ignore
        }
        return d;
    }
    
    /**
     * 当天的系统日期
     * 
     * @return 格式如: 2017-12-01
     */
    public static String getTodayFormat() {
       return getLaterFormat(0);
    }
    
    /**
     * 与当天间隔天数的系统日期
     * 
     * @param days: 间隔天数, 正数为之后，负数为前
     * @return 格式如: 2017-12-01
     */
    public static String getLaterFormat(int days) {
        Calendar c = Calendar.getInstance();
        if(days != 0) {
            c.add(Calendar.DAY_OF_YEAR, days);
        }
        return formatDate(c.getTime());
    }
    
    public static String formatDate(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }
    
    public static Date parseDate(String str) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(str);
        } catch (Exception e) {
            // ignore
        }
        return date;
    }
    
    public static String formatMoney(BigDecimal money) {
        return new DecimalFormat("0.00").format(money);
    }

}
