package elitequant.performance;

import static elitequant.util.UtilFunc.formatDate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import elitequant.gui.TearsheetWindow;
import elitequant.util.Series;

public class TearSheet {

	private Series<Date, BigDecimal> equitys;
	private Map<Date, Map<String, BigDecimal>> positions;
	private Map<Date, Map<String, Object>> trades;

	public TearSheet(Series<Date, BigDecimal> equitys, Map<Date, Map<String, BigDecimal>> positions,
	        Map<Date, Map<String, Object>> trades) {
		this.equitys = equitys;
		this.positions = positions;
		this.trades = trades;
	}

	public void createTearSheet() {

		List<Date> dates = equitys.getKeyList();
		List<BigDecimal> moneys = equitys.getValueList();
		Date startDate = dates.get(0);
		Date endDate = dates.get(dates.size() - 1);

		// calculate the backtest months
		Calendar startC = Calendar.getInstance();
		startC.setTime(startDate);
		Calendar endC = Calendar.getInstance();
		endC.setTime(endDate);
		int months = (endC.get(Calendar.YEAR) - startC.get(Calendar.YEAR)) * 12 + endC.get(Calendar.MONTH)
		        - startC.get(Calendar.MONTH);

		// show message in console
		System.out.println(String.format("Entire data start date: %s", formatDate(startDate)));
		System.out.println(String.format("Entire data end date: %s", formatDate(endDate)));
		System.out.println(String.format("Backtest months: %d", months));
		TearsheetWindow tw = new TearsheetWindow(dates, moneys);

		// show ui
		tw.pack();
		tw.setVisible(true);
	}

}
