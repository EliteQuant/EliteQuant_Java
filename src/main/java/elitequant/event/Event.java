package elitequant.event;

/**
 * Base Event class for event-driven system
 * @author btcaimail@163.com
 *
 */
public abstract class Event {
	
	private EventType eventType;
	
	public Event(EventType eventType){
		this.eventType = eventType;
	}
	
	private String getTypename(){
		return eventType.name();
	}

    public EventType getEventType() {
        return eventType;
    }
	
	

}
