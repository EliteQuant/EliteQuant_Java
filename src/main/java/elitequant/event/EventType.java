package elitequant.event;

public enum EventType {

    TICK,
    BAR,
    ORDER,
    FILL,
    CANCEL,
    ORDERSTATUS,
    ACCOUNT,
    POSITION,
    TIMER,
    GENERAL;

}
