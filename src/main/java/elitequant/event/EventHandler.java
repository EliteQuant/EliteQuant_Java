package elitequant.event;

public interface EventHandler {
    
    void handle(Event event);

}
