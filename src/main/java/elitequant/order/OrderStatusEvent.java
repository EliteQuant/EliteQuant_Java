package elitequant.order;

import elitequant.event.Event;
import elitequant.event.EventType;

public class OrderStatusEvent extends Event{
    

    public int internalOrderId = -1;
    public int brokerOrderId = -1;
    public String fullSymbol = "";
    public OrderStatus orderStatus = OrderStatus.NONE;

    public OrderStatusEvent() {
        super(EventType.ORDERSTATUS);
    }
}
