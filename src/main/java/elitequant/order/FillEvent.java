package elitequant.order;

import java.math.BigDecimal;
import java.util.Date;

import elitequant.event.Event;
import elitequant.event.EventType;
import elitequant.position.Position;
import elitequant.util.UtilFunc;

/**
 * Fill event, with filled quantity/size and price
 * 
 */
public class FillEvent extends Event {

    public int internalOrderId = -1;
    public int brokerOrderId = -1;
    public Date timestamp;
    public String fullSymbol;
    public BigDecimal fillPrice;
    public int fillSize;     // size < 0 means short order is filled
    public String exchange;
    public BigDecimal commission;
    
    public FillEvent() {
        super(EventType.FILL);
        
        internalOrderId = -1;
        brokerOrderId = -1;
        timestamp = UtilFunc.getDate19700101();
        fullSymbol = "";
        exchange = "";
    }
    

    /**
     *  if there is no existing position for this symbol, this fill will create a new position
     *  (otherwise it will be adjusted to exisitng position)
        
     * @return
     */
    public Position toPosition() {
        BigDecimal averagePriceIncludingCommission = null;
        if(fillSize > 0) {
            averagePriceIncludingCommission = fillPrice.add(commission.divide(UtilFunc.retrieveMultiplierFromFullSymbol(fullSymbol)));
        }
        else {
            averagePriceIncludingCommission = fillPrice.subtract(commission.divide(UtilFunc.retrieveMultiplierFromFullSymbol(fullSymbol)));
        }

        Position newPosition = new Position(fullSymbol, averagePriceIncludingCommission, fillSize);
        return newPosition;
    }
}
