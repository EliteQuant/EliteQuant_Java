package elitequant.order;

/**
 * OrderType.MARKET.name == "MARKET"  OderType.MARKET.value == 0
 *
 */
public enum OrderType {

	MARKET(0),
	LIMIT(2),
	STOP(5),
	STOP_LIMIT(6),
	TRAIING_STOP(7);
    
    public String name;
    public int value;
    
    OrderType(int value){
    	this.name = name();
    	this.value = value;
    }
}
