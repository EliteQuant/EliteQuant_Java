package elitequant.data;

import java.math.BigDecimal;
import java.util.Date;

import elitequant.event.Event;
import elitequant.event.EventType;
import elitequant.util.UtilFunc;

/**
 * Bar event, aggregated from TickEvent
 *
 */
public class BarEvent extends Event {
	
    public Date barStartTime;
    public int interval;
    public String fullSymbol;
    public BigDecimal openPrice;
    public BigDecimal highPrice;
    public BigDecimal lowPrice;
    public BigDecimal closePrice;
    public BigDecimal adjClosePrice;
    public int volume;
	
	public BarEvent(){
		super(EventType.BAR);

		barStartTime = UtilFunc.getDate19700101();
		interval = 86400; // 1day in secs = 24hrs * 60min * 60sec
		fullSymbol = "";
	}
	
	public Date barEndTime(){
		// To be consistent with (daily) bar backtest, bar_end_time is set to be bar_start_time
		return barStartTime;
	}

	@Override
	public String toString() {
		return String.format("Time: %tc, Symbol: %s, Interval: %d, Open: %.2f, High: %.2f, Low: %.2f, Close: %.2f, Adj Close: %.2f, Volume: %d", 
				barStartTime, fullSymbol, interval, 
				openPrice, highPrice, lowPrice, 
				closePrice, adjClosePrice, volume);
	}
	
	public static void main(String[] args){
		System.out.print(new BarEvent());
	}
	
	

}
