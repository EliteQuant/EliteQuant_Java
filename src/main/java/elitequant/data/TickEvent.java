package elitequant.data;

import java.math.BigDecimal;
import java.util.Date;

import elitequant.event.Event;
import elitequant.event.EventType;
import elitequant.util.UtilFunc;

/**
 * Tick even
 *
 */
public class TickEvent extends Event {
	
    private TickType tickType;
    public Date timestamp;
    public String fullSymbol;
    public BigDecimal price;
    public int size;
	
	public TickEvent() {
		super(EventType.TICK);
		
		tickType = TickType.TRADE;
		fullSymbol = "";
		timestamp = UtilFunc.getDate19700101();
	}
	
	

    @Override
	public String toString() {
        return String.format("Time: %tc, Ticker: %s, Type: %s,  Price: %.2f, Size %d", 
                timestamp, fullSymbol, tickType,
                price, size
            );
	}

}
