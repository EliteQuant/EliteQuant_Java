package elitequant.data;

import static elitequant.util.UtilFunc.getTodayFormat;
import static elitequant.util.UtilFunc.parseDate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import elitequant.util.UtilFunc;

public class BacktestDataFeedQuandl extends DataFeedBase {

    private final static String QUANDL_API = "https://www.quandl.com/api/v3/datasets/WIKI/%s/data.json?order=asc&api_key=ay68s2CUzKbVuy8GAqxj";
    
    private String startDate;
    private String endDate;
    private List<BarEvent> histData;
    private Iterator<BarEvent> dataStream;
    
    public BacktestDataFeedQuandl(String startDate, String endDate) {
        if(endDate != null){
            this.endDate = endDate;
        }
        else{
            this.endDate = UtilFunc.getTodayFormat();
        }
        
        if(startDate != null){
            this.startDate = startDate;
        }
        else{
            this.startDate = UtilFunc.getLaterFormat(-365);
        }
        
        histData = new ArrayList<BarEvent>();
    }


    @Override
    public void subscribeMarketData(String[] symbols) {
        if(symbols != null) {
            for(String sym : symbols) {
                retrieveOnlineHistoricalData(sym);
            }
        }
        dataStream = histData.iterator();
        
    }


    @Override
    public void unsubscribeMarketData(String[] symbols) {
    }


    @Override
    public BarEvent streamNext() {
        if(dataStream.hasNext()) {
            return dataStream.next();
        }
        else {
            return null;
        }
    }

    private void retrieveOnlineHistoricalData(String symbol) {
        
        StringBuilder url = new StringBuilder(String.format(QUANDL_API, symbol));
        
        // filter
        if(startDate != null) {
            url.append(String.format("&start_date=%s", startDate));
        }
        if(endDate != null) {
            url.append(String.format("&end_date=%s", endDate));
        }
        
        String json = httpGet(url.toString());
        JSONObject obj = new JSONObject(json);
        JSONObject dataset = obj.getJSONObject("dataset_data");
        JSONArray record = dataset.getJSONArray("data");
        
        for (int i = 0; i < record.length(); i++) {
            JSONArray item = record.getJSONArray(i);
            
            BarEvent bar = new BarEvent();
            bar.fullSymbol = symbol;
            bar.interval = 86400;
            bar.barStartTime = UtilFunc.parseDate(item.getString(0));
            bar.openPrice = item.getBigDecimal(1);
            bar.highPrice = item.getBigDecimal(2);
            bar.closePrice = item.getBigDecimal(3);
            bar.lowPrice = item.getBigDecimal(4);
            bar.volume = item.getBigDecimal(5).intValue();
            bar.adjClosePrice = item.getBigDecimal(8);
            histData.add(bar);
        }
    }
    

}
