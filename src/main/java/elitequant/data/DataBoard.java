package elitequant.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Data tracker that holds current market data info
 * 
 */
public class DataBoard {
	
	private Map<String, Map> symbols;
	
	public DataBoard() {
		symbols = new HashMap<String, Map>();
	}
	
	public void onTick(TickEvent tickEvent) {
	    synchronized (symbols) {
	        Map data = symbols.get(tickEvent.fullSymbol);
	        if(data == null){
	            data = new HashMap();
	            symbols.put(tickEvent.fullSymbol, data);
	        }
	        data.put("timestamp", tickEvent.timestamp);
	        data.put("last_price", tickEvent.price);
        }
	}
	
	public void onBar(BarEvent barEvent){
	    synchronized (symbols) {
	        Map data = symbols.get(barEvent.fullSymbol);

	        if(data == null){
	            data = new HashMap();
	            symbols.put(barEvent.fullSymbol, data);
	        }

	        data.put("timestamp", barEvent.barEndTime());
	        data.put("last_price", barEvent.closePrice);
	        data.put("last_adj_price", barEvent.adjClosePrice);
	    }
	}

	/**
	 * Returns the most recent actual timestamp for a given ticker
	 * @param symbol
	 * @return
	 */
    public BigDecimal getLastPrice(String symbol){
        Map data = symbols.get(symbol);
        if(data != null){
        	return (BigDecimal)data.get("last_adj_price");
        }
            
        else{
        	System.out.println(String.format("LastPrice for ticker %s is not found", symbol));
        	return null;
        }
    }
    

    /**
     * Returns the most recent actual timestamp for a given ticker
     * @param symbol
     * @return
     */
    public Date getLastTimestamp(String symbol) {
        if(symbols.containsKey(symbol)){
            return (Date)symbols.get(symbol).get("timestamp");
       }
        else {
            System.out.println(String.format("Timestamp for ticker %s is not found", symbol));
            return null;
        }
    }

}
