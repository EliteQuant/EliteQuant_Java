package elitequant.util;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import nanomsg.pubsub.SubSocket;

public class Test extends JFrame {
    public static void main(String[] args){
        SubSocket sock = new SubSocket();
        sock.connect("tcp://127.0.0.1:55555");
        sock.subscribe("s");
        System.out.println(sock.recvString());
    }
}

