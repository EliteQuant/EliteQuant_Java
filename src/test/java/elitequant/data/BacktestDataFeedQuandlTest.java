package elitequant.data;

public class BacktestDataFeedQuandlTest {
    
    public static void main(String[] args) {
        BacktestDataFeedQuandl dataFeed = new BacktestDataFeedQuandl("2015-05-24", "2015-05-28");
        dataFeed.subscribeMarketData(new String[] {"AMZN"});
        BarEvent bar = null;
        while((bar = dataFeed.streamNext()) != null) {
            System.out.println(bar);
        }
        
    }

}
