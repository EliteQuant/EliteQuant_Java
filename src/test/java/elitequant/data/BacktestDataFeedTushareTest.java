package elitequant.data;

public class BacktestDataFeedTushareTest {
    
    public static void main(String[] args) {
        BacktestDataFeedTushare dataFeed = new BacktestDataFeedTushare("2014-12-03", "2014-12-05");
        dataFeed.subscribeMarketData(new String[] {"sh000001"});
        BarEvent bar = null;
        while((bar = dataFeed.streamNext()) != null) {
            System.out.println(bar);
        }

    }

}
